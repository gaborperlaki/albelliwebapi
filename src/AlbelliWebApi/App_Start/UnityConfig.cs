using AlbelliWebApi.Repository;
using AlbelliWebApi.Services;
using System.Web.Http;
using Unity;
using Unity.WebApi;

namespace AlbelliWebApi
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<IRepository, Repository.Repository>();
            container.RegisterType<ICustomerService, CustomerService>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}