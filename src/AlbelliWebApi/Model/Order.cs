﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AlbelliWebApi.Model
{
    public class Order
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OrderId { get; set; }

        public int CustomerId { get; set; }

        public decimal Price { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}