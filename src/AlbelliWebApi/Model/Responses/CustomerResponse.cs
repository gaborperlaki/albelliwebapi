﻿namespace AlbelliWebApi.Model.Responses
{
    public class CustomerResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }
    }
}