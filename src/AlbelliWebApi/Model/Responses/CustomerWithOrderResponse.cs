﻿using System.Collections.Generic;

namespace AlbelliWebApi.Model.Responses
{
    public class CustomerWithOrderResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public IEnumerable<Order> Orders { get; set; }
    }
}