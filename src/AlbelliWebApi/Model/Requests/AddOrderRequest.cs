﻿using System;

namespace AlbelliWebApi.Model.Requests
{
    public class AddOrderRequest
    {
        public int OrderId { get; set; }

        public int CustomerId { get; set; }

        public decimal Price { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}