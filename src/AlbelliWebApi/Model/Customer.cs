﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AlbelliWebApi.Model
{
    public class Customer
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }
    }
}