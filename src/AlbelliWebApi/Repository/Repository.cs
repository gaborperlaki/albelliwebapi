﻿using System;
using System.Collections.Generic;
using System.Linq;
using AlbelliWebApi.Model;
using System.Data.Entity;

namespace AlbelliWebApi.Repository
{
    public class Repository : IRepository
    {
        private readonly AlbelliDbContext _dbContext;

        public Repository()
        {
            _dbContext = new AlbelliDbContext();
        }

        public virtual void AddCustomer(Customer customer)
        {
            var existingCustomer = GetCustomer(customer.Id);

            if (existingCustomer != null)
                throw new InvalidOperationException($"Customer already exists for with id={customer.Id}");

            _dbContext.Customers.Add(customer);
            _dbContext.SaveChanges();
        }

        public virtual void AddOrderToCustomer(Order order)
        {
            var existingCustomer = GetCustomer(order.CustomerId);

            if (existingCustomer == null)
                throw new InvalidOperationException($"Customer does not exist id={order.CustomerId}");

            var ordersForCustomer = GetOrdersForCustomer(order.CustomerId).ToList();

            if (ordersForCustomer.Contains(order))
                throw new InvalidOperationException($"Order already exists for customer id={order.CustomerId}");

            _dbContext.Orders.Add(order);
            _dbContext.SaveChanges();
        }

        public virtual Customer GetCustomer(int id)
        {
            return _dbContext.Customers.SingleOrDefault(c => c.Id == id);
        }

        public virtual IEnumerable<Customer> GetAllCustomers()
        {
            return _dbContext.Customers;
        }

        public virtual IEnumerable<Order> GetOrdersForCustomer(int customerId)
        {
            return _dbContext.Orders.Where(o => o.CustomerId == customerId);
        }

        public virtual void DeleteCustomer(int customerId)
        {
            var customer = GetCustomer(customerId);

            if (customer == null)
            {
                //log
                return;
            }

            _dbContext.Entry(customer).State = EntityState.Deleted;
            _dbContext.SaveChanges();
        }

        public virtual void DeleteOrder(int orderId)
        {
            var order = _dbContext.Orders.SingleOrDefault(o => o.OrderId == orderId);

            if (order == null)
            {
                //log
                return;
            }

            _dbContext.Entry(order).State = EntityState.Deleted;
            _dbContext.SaveChanges();
        }
    }
}