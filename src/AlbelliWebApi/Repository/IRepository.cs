﻿using AlbelliWebApi.Model;
using System.Collections.Generic;

namespace AlbelliWebApi.Repository
{
    public interface IRepository
    {
        Customer GetCustomer(int id);

        IEnumerable<Customer> GetAllCustomers();

        void AddCustomer(Customer customer);

        void AddOrderToCustomer(Order order);

        IEnumerable<Order> GetOrdersForCustomer(int customerId);

        void DeleteCustomer(int customerId);

        void DeleteOrder(int orderId);
    }
}
