﻿using AlbelliWebApi.Model.Requests;
using AlbelliWebApi.Model.Responses;
using AlbelliWebApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;


namespace AlbelliWebApi.Controllers
{
    [RoutePrefix("api/customer")]
    public class CustomerController : ApiController
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IHttpActionResult> GetCustomer(int id)
        {
            CustomerWithOrderResponse result;

            try
            {
                result = _customerService.GetCustomer(id);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            if (result != null)
            {
                return Ok(result);
            }

            return NotFound();
        }

        [HttpGet]
        [Route("allwithoutorders")]
        public async Task<IHttpActionResult> GetCustomersWithoutOrders()
        {
            IEnumerable<CustomerResponse> result;

            try
            {
                result = _customerService.GetAllCustomersWithoutOrders();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            if (result.Any())
            {
                return Ok(result);
            }

            return NotFound();
        }

        [HttpPost]
        [Route("addcustomer")]
        public async Task<IHttpActionResult> AddCustomer([FromBody]AddCustomerRequest request)
        {
            try
            {
                _customerService.AddCustomer(request);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            return Ok();
        }

        [HttpPost]
        [Route("addordertocustomer")]
        public async Task<IHttpActionResult> AddOrder([FromBody]AddOrderRequest request)
        {
            try
            {
                _customerService.AddOrderForCustomer(request);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            return Ok();
        }
    }
}
