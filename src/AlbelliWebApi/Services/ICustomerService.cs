﻿using AlbelliWebApi.Model.Requests;
using AlbelliWebApi.Model.Responses;
using System.Collections.Generic;

namespace AlbelliWebApi.Services
{
    public interface ICustomerService
    {
        CustomerWithOrderResponse GetCustomer(int id);

        IEnumerable<CustomerResponse> GetAllCustomersWithoutOrders();

        void AddCustomer(AddCustomerRequest request);

        void AddOrderForCustomer(AddOrderRequest request);
    }
}
