﻿using System.Collections.Generic;
using AlbelliWebApi.Model;
using AlbelliWebApi.Repository;
using AlbelliWebApi.Model.Requests;
using AlbelliWebApi.Model.Responses;

namespace AlbelliWebApi.Services
{
    public class CustomerService : ICustomerService
    {
        private IRepository _repository;

        public CustomerService(IRepository repository)
        {
            _repository = repository;
        }

        public void AddCustomer(AddCustomerRequest request)
        {
            var customer = new Customer
            {
                Id = request.Id,
                Name = request.Name,
                Email = request.Email
            };

            _repository.AddCustomer(customer);
        }

        public void AddOrderForCustomer(AddOrderRequest request)
        {
            var order = new Order
            {
                OrderId = request.OrderId,
                CustomerId = request.CustomerId,
                Price = request.Price,
                CreatedDate = request.CreatedDate
            };

            _repository.AddOrderToCustomer(order);
        }

        public CustomerWithOrderResponse GetCustomer(int id)
        {
            var customer = _repository.GetCustomer(id);

            var orders = _repository.GetOrdersForCustomer(id);

            return new CustomerWithOrderResponse
            {
                Id = customer.Id,
                Name = customer.Name,
                Email = customer.Email,
                Orders = orders
            };
        }

        public IEnumerable<CustomerResponse> GetAllCustomersWithoutOrders()
        {
            var customers = _repository.GetAllCustomers();

            foreach (var customer in customers)
            {
                yield return new CustomerResponse
                {
                    Id = customer.Id,
                    Name = customer.Name,
                    Email = customer.Email
                };
            }
        }
    }
}