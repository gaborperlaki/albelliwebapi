﻿using AlbelliWebApi.Model;
using AlbelliWebApi.Model.Requests;
using AlbelliWebApi.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliWebApi.Tests.Unit
{
    [TestFixture]
    public class CustomerServiceTests
    {
        private Mock<Repository.Repository> _repositoryMock;
        private ICustomerService _sut;

        [SetUp]
        public void SetUp()
        {
            _repositoryMock = new Mock<Repository.Repository>();
            _sut = new CustomerService(_repositoryMock.Object);
        }

        [Test]
        public void GivenANewCustomer_WhenISendAnAddCustomerRequest_ThenCustomerShouldBeCreated()
        {
            _repositoryMock
                .Setup(m => m.AddCustomer(It.IsAny<Customer>()));

            _sut.AddCustomer(new AddCustomerRequest { Id = 1, Name = "x Y", Email = "xy@gmail.com" });

            _repositoryMock.Verify(m => m.AddCustomer(It.IsAny<Customer>()), Times.Once());
        }

        [Test]
        public void GivenANewCustomer_WhenIWantToCreateACustomerWithExistingId_ThenExceptionShouldBeThrown()
        {
            _repositoryMock
                .Setup(m => m.AddCustomer(It.IsAny<Customer>()))
                .Throws(new InvalidOperationException());

            Assert.Throws<InvalidOperationException>(() => _sut.AddCustomer(new AddCustomerRequest { Id = 1, Name = "x Y", Email = "xy@gmail.com" }));
        }

        [Test]
        public void GivenANewOrderForCustomer_WhenISendAnAddOrderRequest_ThenOrderForCustomerShouldBeCreated()
        {
            _repositoryMock
                .Setup(m => m.AddOrderToCustomer(It.IsAny<Order>()));

            _sut.AddOrderForCustomer(new AddOrderRequest { OrderId = 1, CustomerId = 1, Price = 100, CreatedDate = DateTime.Now });

            _repositoryMock.Verify(m => m.AddOrderToCustomer(It.IsAny<Order>()), Times.Once());
        }

        [Test]
        public void GivenANewOrderForCustomer_WhenISendAnAddOrderRequestForANonExistingCustomer_ThenExceptionShouldBeThrown()
        {
            _repositoryMock
                .Setup(m => m.AddOrderToCustomer(It.IsAny<Order>()))
                .Throws(new InvalidOperationException());

            Assert.Throws<InvalidOperationException>(() => _sut.AddOrderForCustomer(new AddOrderRequest { OrderId = 1, CustomerId = 1, Price = 100, CreatedDate = DateTime.Now }));
        }

        [Test]
        public void GivenAllCustomers_WhenIGetACustomerById_ThenCustomerWithOrdersShouldBeReturned()
        {
            int customerId = 1;

            _repositoryMock
                .Setup(m => m.GetCustomer(customerId))
                .Returns(new Customer { Id = customerId });

            _repositoryMock
                .Setup(m => m.GetOrdersForCustomer(customerId))
                .Returns(new List<Order> { new Order() { OrderId = 1, CustomerId = customerId } });

            var customer = _sut.GetCustomer(customerId);

            Assert.AreEqual(customer.Id, customerId);
            Assert.IsNotEmpty(customer.Orders);
        }

        [Test]
        public void GivenAllCustomers_WhenIGetAllCustomersWithoutOrders_ThenCustomerWithoutOrdersShouldOnlyBeReturned()
        {
            _repositoryMock
                .Setup(m => m.GetAllCustomers())
                .Returns(new List<Customer> { new Customer() });

            var customers = _sut.GetAllCustomersWithoutOrders();

            Assert.AreNotEqual(customers.Count(), 0);
        }
    }
}
