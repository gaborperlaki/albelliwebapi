﻿using NUnit.Framework;
using AlbelliWebApi.Repository;
using AlbelliWebApi.Model;
using System;
using System.Linq;

namespace AlbelliWebApi.Tests.Acceptance
{
    [TestFixture]
    public class RepositoryTests
    {
        private IRepository _sut;

        [SetUp]
        public void SetUp()
        {
            _sut = new Repository.Repository();

            var customer1 = new Customer { Id = 1, Name = "Stefan Mustermann", Email = "stefan@gmail.com" };
            var customer2 = new Customer { Id = 2, Name = "Hans Zimmermann", Email = "hans@gmail.com" };
            var customer3 = new Customer { Id = 3, Name = "Stefania Mustermann", Email = "stefania@gmail.com" };

            _sut.AddCustomer(customer1);
            _sut.AddCustomer(customer2);
            _sut.AddCustomer(customer3);

            var order = new Order { OrderId = 1, Price = 100, CustomerId = 2, CreatedDate = DateTime.Now };

            _sut.AddOrderToCustomer(order);
        }

        [Test]
        public void GivenNewCustomer_WhenICreateNewCustomer_ItWillBeSavedIntoTheDatabaseSuccessfully()
        {
            var savedCustomer = _sut.GetCustomer(1);

            Assert.AreEqual(savedCustomer.Name, "Stefan Mustermann");
            Assert.AreEqual(savedCustomer.Email, "stefan@gmail.com");
        }

        [Test]
        public void GivenCustomers_WhenIRequestAllCustomersWithoutOrders_ThenIGetRelevantCustomersOnly()
        {
            var customersWithoutOrders = _sut.GetAllCustomers();

            Assert.AreEqual(customersWithoutOrders.Count(), 3);
        }

        [TearDown]
        public void TearDown()
        {
            _sut.DeleteCustomer(1);
            _sut.DeleteCustomer(2);
            _sut.DeleteCustomer(3);

            _sut.DeleteOrder(1);
        }
    }
}
