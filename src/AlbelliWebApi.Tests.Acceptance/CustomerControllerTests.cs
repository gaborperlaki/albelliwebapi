﻿using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Linq;
using AlbelliWebApi.Model.Responses;
using AlbelliWebApi.Model.Requests;
using System;

namespace AlbelliWebApi.Tests.Acceptance
{
    [TestFixture]
    public class CustomerControllerTests
    {
        private readonly string _serviceUrl = ConfigurationManager.AppSettings["AlbelliApiServiceUrl"];

        private RestClient _restClient;

        [SetUp]
        public void SetUp()
        {
            _restClient = new RestClient(_serviceUrl);

            AddCustomer(new AddCustomerRequest { Id = 100, Name = "Adam Test", Email = "adam.test@gmail.com" });
            AddCustomer(new AddCustomerRequest { Id = 101, Name = "Eva Test", Email = "eva.test@gmail.com" });
            AddCustomer(new AddCustomerRequest { Id = 102, Name = "Frances Test", Email = "frances.test@gmail.com" });

            AddOrder(new AddOrderRequest { OrderId = 100, CustomerId = 100, CreatedDate = DateTime.Now, Price = 1000 });
        }

        [Test]
        public void GivenAllCustomers_WhenIRequestACustomerWithId_ThenIGetTheExactCustomerWithOrdersSuccessfully()
        {
            int testedCustomerId = 100;

            var request = new RestRequest($"api/customer/{testedCustomerId}", Method.GET);

            var response = _restClient.Execute(request);

            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);

            var results = JsonConvert.DeserializeObject<CustomerWithOrderResponse>(response.Content);

            Assert.AreEqual(results.Id, testedCustomerId);
            Assert.IsNotEmpty(results.Orders);
        }

        [Test]
        public void GivenAllCustomers_WhenIRequestAllCustomersWithoutOrders_ThenIGetAllCustomerWithoutOrdersSuccessfully()
        {
            var request = new RestRequest($"api/customer/allwithoutorders", Method.GET);

            var response = _restClient.Execute(request);

            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);

            var results = JsonConvert.DeserializeObject<IEnumerable<CustomerResponse>>(response.Content);

            Assert.AreEqual(results.Count(), 2);

            var validIds = new List<int>() { 101, 102 };

            validIds.ForEach(id =>
                Assert.Contains(id, results.Select(r => r.Id).ToList()));
        }

        [Test]
        public void GivenANewCustomer_WhenISendAnAddCustomerRequest_ThenCustomerIsAddedToTheDatabaseSuccessfully()
        {
            var request = new AddCustomerRequest { Id = 103, Name = "Diana Test", Email = "diana.test@gmail.com" };
            var response = AddCustomer(request);

            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
        }

        [Test]
        public void GivenANewOrderForCustomer_WhenISendAnAddOrderRequest_ThenOrderForCustomerIsAddedToTheDatabaseSuccessfully()
        {
            var request = new AddOrderRequest { OrderId = 101, CustomerId = 103, CreatedDate = DateTime.Now, Price = 3000 };
            var response = AddOrder(request);

            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
        }


        private IRestResponse AddCustomer(AddCustomerRequest req)
        {
            var request = new RestRequest($"api/customer/addcustomer", Method.POST);
            request.AddParameter("text/json", JsonConvert.SerializeObject(req), ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;

            var response = _restClient.Execute(request);

            return response;
        }

        private IRestResponse AddOrder(AddOrderRequest req)
        {
            var request = new RestRequest($"api/customer/addordertocustomer", Method.POST);
            request.AddParameter("text/json", JsonConvert.SerializeObject(req), ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;

            var response = _restClient.Execute(request);

            return response;
        }
    }
}
